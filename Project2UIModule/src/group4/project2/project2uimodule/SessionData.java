package group4.project2.project2uimodule;

import java.io.Serializable;

import android.app.Application;

public class SessionData extends Application implements Serializable {
	private static final long serialVersionUID = 1L;
	public String username;
	public String password;
	public int selectedEvent;
	public int selectedGuest;
	public int selectedItem;
	
	public Event[] syncedEvents;
	
	public SessionData() {
		syncedEvents = new Event[0];	
	}
	
	static class Event implements Serializable {
		private static final long serialVersionUID = 1L;
		public String eventName;
		public String eventDate;
		public String eventDescription;
		public EventGuest[] eventGuests;
		public EventItem[] eventItems;
		
		Event(){
			eventName = new String();
			eventDate = new String();
			eventDescription = new String();
			eventGuests = new EventGuest[0];
			eventItems = new EventItem[0];
		}
	}
	
	static class EventGuest implements Serializable{
		private static final long serialVersionUID = 1L;
		public String guestName;
		public EventItem[] guestItems;
		
		EventGuest(){
			guestName = new String();
			guestItems = new EventItem[0];
		}
	}
	
	static class EventItem implements Serializable{
		private static final long serialVersionUID = 1L;
		public String itemName;
		
		EventItem(){
			itemName = new String();
		}
	}
}
