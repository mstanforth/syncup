package group4.project2.project2uimodule;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;

import com.scringo.Scringo;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Toast;

public class CreateEvent extends Activity {
	//UI Module Global Variables
		public EditText eventNameEditText;
		public EditText eventDescriptionEditText;
		public ListView eventItemsListView;
		public ListView eventGuestsListView;
		public Button addItemsButton;
		public Button addGuestsButton;
		public Button createEventButton;
		public SessionData sessionData;
		//End UI Global Variables
		
		private Scringo scringo;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_create_event);
		
		Scringo.setAppId("ZhbBOvc9ZxVrmGQjw4DZHNIvMsnS9N5R");
		scringo = new Scringo(this);
		scringo.init();
		scringo.addSidebar();
		
		//UI Module onCreate functionality
		final Intent createEventIntent = new Intent(this, SyncedEvents.class);
		final Intent addItemIntent = new Intent(this, AddItem.class);
		final Intent addGuestIntent = new Intent(this, AddGuest.class);
		
		Bundle extras = getIntent().getExtras();
		if (extras != null) {
		   sessionData = (SessionData)extras.getSerializable("Session Data");
		}
		
		
		createEventButton = (Button)findViewById(R.id.createEventButton);
		addItemsButton = (Button)findViewById(R.id.createEventAddEventItemsButton);
		addGuestsButton = (Button)findViewById(R.id.createEventAddEventGuestsButton);
		eventGuestsListView = (ListView)findViewById(R.id.createEventGuestsListView);
		eventItemsListView = (ListView)findViewById(R.id.createEventItemsListView);
		eventNameEditText = (EditText)findViewById(R.id.createEventEventNameEditText);
		eventDescriptionEditText = (EditText)findViewById(R.id.createEventEventDescriptionEditText);
		
		createEventButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				try {
					ValidateEventName(createEventIntent);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		
		addItemsButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				addItemIntent.putExtra("Session Data", sessionData);
				startActivity(addItemIntent);
				finish();
			}
		});
		
		addGuestsButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				addGuestIntent.putExtra("Session Data", sessionData);
				startActivity(addGuestIntent);
				finish();
			}
		});
		
		eventGuestsListView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
					//sessionData.selectedGuest = position;
					//isBringingIntent.putExtra("Session Data", sessionData);
					//startActivity(isBringingIntent);
					//finish();
			}
		});
		
		ArrayList<String> eventGuestsList = new ArrayList<String>();    
	    ArrayAdapter<String> listAdapter = new ArrayAdapter<String>(this, R.layout.simple_row, eventGuestsList); 
		    for(int i=0; i<sessionData.syncedEvents[sessionData.selectedEvent].eventGuests.length; ++i){
		    	listAdapter.add(sessionData.syncedEvents[sessionData.selectedEvent].eventGuests[i].guestName);
		    }
	    eventGuestsListView.setAdapter(listAdapter);
		
		ArrayList<String> eventItemsList = new ArrayList<String>();    
	    ArrayAdapter<String> listAdapter2 = new ArrayAdapter<String>(this, R.layout.simple_row, eventItemsList); 
	    for(int i=0; i<sessionData.syncedEvents[sessionData.selectedEvent].eventItems.length; ++i){
	    	listAdapter2.add(sessionData.syncedEvents[sessionData.selectedEvent].eventItems[i].itemName);
	    }
	    eventItemsListView.setAdapter(listAdapter2);
		//End UI Module onCreate functionality
	}
	public void ValidateEventName(Intent intent) throws IOException{
		if(eventNameEditText.getText().toString().equals("")){
			Toast.makeText(this, "Invalid Event Name", Toast.LENGTH_LONG).show();
		}
		else{
			sessionData.syncedEvents[sessionData.selectedEvent].eventName = eventNameEditText.getText().toString();
			//sessionData.syncedEvents[sessionData.selectedEvent].eventDate = eventNameEditText.getText().toString();
			sessionData.syncedEvents[sessionData.selectedEvent].eventDescription = eventDescriptionEditText.getText().toString();
			intent.putExtra("Session Data", sessionData);
			
			// store stuff here 
			String data = sessionData.syncedEvents[sessionData.selectedEvent].eventName + 
					sessionData.syncedEvents[sessionData.selectedEvent].eventDate +
					sessionData.syncedEvents[sessionData.selectedEvent].eventDescription;
			
			writeToFile(data);
			
		 }

			
			startActivity(intent);
			finish();
		}
	
	private void writeToFile(String data) {
	    try {
	        OutputStreamWriter outputStreamWriter = new OutputStreamWriter(openFileOutput("events.txt", Context.MODE_PRIVATE));
	        outputStreamWriter.write(data);
	        outputStreamWriter.close();
	    }
	    catch (IOException e) {
	        Log.e("Exception", "File write failed: " + e.toString());
	    } 
	}

	@Override
	protected void onStart(){
		super.onStart();
		scringo.onStart();
	}
	
	@Override
	protected void onStop(){
		super.onStop();
		scringo.onStop();
	}
	
	
	}



