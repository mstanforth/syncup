package group4.project2.project2uimodule;

import group4.project2.project2uimodule.SessionData.Event;
import group4.project2.project2uimodule.SessionData.EventGuest;
import group4.project2.project2uimodule.SessionData.EventItem;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.FacebookRequestError;
import com.facebook.HttpMethod;
import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionLoginBehavior;
import com.facebook.SessionState;
import com.facebook.internal.SessionAuthorizationType;
import com.facebook.internal.SessionTracker;
import com.facebook.model.GraphObject;
import com.scringo.Scringo;

public class Login extends Activity {
	
	/**
     * App info
     */
	public final String appID = "838092739563865";
	public final String TAG = "Session Trace";
	
	/**
     * widgets
     */
	public Button sendRequestButton;
	public Button getDataButton;

	public TextView dataDisplay;
	public TextView welcomeText;

	public EditText textInput;
	
	/**
     * Permission lists
     */
	String[] permissionsToRead = new String[] {"public_profile", "user_events", "email"};
	String[] permissionsToCreate = new String[] {"create_event", "publish_actions"};
	
	List<String> readPermissionList = Arrays.asList(permissionsToRead);
	List<String> createPermissionList = Arrays.asList(permissionsToCreate);
	
	/**
     * objects and classes
     */
	public Scringo scringo;
	public SessionTracker sessionTracker;
	
	//UI Module Global Variables
	public Button loginButton;
	public EditText userNameEditText;
	public EditText passwordEditText;
	public SessionData sessionData;
	//End UI Global Variables

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
		
		//being session tracking
		sessionTracker = new SessionTracker(this, new DefaultSessionStatusCallback());
		
		Scringo.setAppId("ZhbBOvc9ZxVrmGQjw4DZHNIvMsnS9N5R");
		scringo = new Scringo(this);
		scringo.init();
		scringo.addSidebar();
		
		//UI Module onCreate functionality
		sessionData = new SessionData();
		loginButton = (Button)findViewById(R.id.loginButton);
		loginButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				try {
					ValidateInput();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		//End UI Module onCreate functionality
	}
	
	public void ValidateInput() throws IOException{

			openSessionForRead(appID, readPermissionList);

	}
	
	public void GenerateTestEvent() throws IOException{		
		
		String data = "Dinner and Movie, Nov 17 2014 At 9pm, This is a sample event."; //readFromFile();
		String[] tokens = data.split(",");

		if( tokens[0] != "" ) {
		EventItem testItem = new EventItem();
		testItem.itemName = "Test Item";
		
		EventGuest testGuest = new EventGuest();
		testGuest.guestName = "Test Guest";
		testGuest.guestItems = new EventItem[]{testItem};
		
		Event testEvent = new Event();
		testEvent.eventName = tokens[0];
		testEvent.eventDate = "Today";
		testEvent.eventDescription = tokens[2];
		testEvent.eventGuests = new EventGuest[]{testGuest};
		testEvent.eventItems = new EventItem[]{testItem};
		
		sessionData.syncedEvents = new Event[] {testEvent};
		
		final Intent loginIntent = new Intent(this, SyncedEvents.class);
		loginIntent.putExtra("Session Data", sessionData);
		startActivity(loginIntent);
		finish();
		}
	}
	
	public String readFromFile() {

	    String ret = "";

	    try {
	        InputStream inputStream = openFileInput("events.txt");

	        if ( inputStream != null ) {
	            InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
	            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
	            String receiveString = "";
	            StringBuilder stringBuilder = new StringBuilder();

	            while ( (receiveString = bufferedReader.readLine()) != null ) {
	                stringBuilder.append(receiveString);
	            }

	            inputStream.close();
	            ret = stringBuilder.toString();
	        }
	    }
	    catch (FileNotFoundException e) {
	        Log.e("login activity", "File not found: " + e.toString());
	    } catch (IOException e) {
	        Log.e("login activity", "Can not read file: " + e.toString());
	    }

	    return ret;
	}
	
	@Override
	protected void onStart(){
		super.onStart();
		scringo.onStart();
	}
	
	@Override
	protected void onStop(){
		super.onStop();
		scringo.onStop();
	}
	/**
     * Called when the activity that was launched exits. This method manages session
     * information when a session is opened. If this method is overridden in subclasses,
     * be sure to call {@code super.onActivityResult(...)} first.
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.i(TAG, "onActivityResult\ndata: " + data.toString() + "\nresult code: " + resultCode + "\n");
        sessionTracker.getSession().onActivityResult(this, requestCode, resultCode, data);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.i(TAG, "onDestroy");
        sessionTracker.stopTracking();
    }

    /**
     * Use the supplied Session object instead of the active Session.
     *
     * @param newSession the Session object to use
     */
    public void setSession(Session newSession) {
    	Log.i(TAG, "setSession");
        if (sessionTracker != null) {
        	Log.i(TAG, "setSession: sessionTracker not null");
            sessionTracker.setSession(newSession);
        }
    }

    // METHOD TO BE OVERRIDDEN
    
    /**
     * Called when the session state changes. Override this method to take action
     * on session state changes.
     * 
     * @param state the new state
     * @param exception any exceptions that occurred during the state change
     */
    protected void onSessionStateChange(SessionState state, Exception exception) {
    	Log.i(TAG, "onSessionStateChange");
    	try {
			GenerateTestEvent();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

    // ACCESSORS (CANNOT BE OVERRIDDEN)
    
    /**
     * Gets the current Session.
     * 
     * @return the current Session object.
     */
    protected final Session getSession() {
    	Log.i(TAG, "getSession");
        if (sessionTracker != null) {
        	Log.i(TAG, "getSession: not null");
            return sessionTracker.getSession();
        }
        return null;
    }

    /**
     * Determines whether the current session is open.
     * 
     * @return true if the current session is open
     */
    protected final boolean isSessionOpen() {
    	Log.i(TAG, "isSessionOpen");
        if (sessionTracker != null) {
        	Log.i(TAG, "isSessionOpen: sessionTracker not null");
            return sessionTracker.getOpenSession() != null;
        }
        return false;
    }
    
    /**
     * Gets the current state of the session or null if no session has been created.
     * 
     * @return the current state of the session
     */
    protected final SessionState getSessionState() {
    	Log.i(TAG, "getSessionState");
        if (sessionTracker != null) {
        	Log.i(TAG, "getSessionState: not null");
            Session currentSession = sessionTracker.getSession();
            return (currentSession != null) ? currentSession.getState() : null;
        }
        return null;
    }
    
    /**
     * Gets the access token associated with the current session or null if no 
     * session has been created.
     * 
     * @return the access token
     */
    protected final String getAccessToken() {
    	Log.i(TAG, "getAccessToken");
        if (sessionTracker != null) {
        	Log.i(TAG, "getAccessToken: sessionTracker not null");
            Session currentSession = sessionTracker.getOpenSession();
            return (currentSession != null) ? currentSession.getAccessToken() : null;
        }
        return null;
    }

    /**
     * Gets the date at which the current session will expire or null if no session 
     * has been created.
     * 
     * @return the date at which the current session will expire
     */
    protected final Date getExpirationDate() {
    	Log.i(TAG, "getExpirationDate");
        if (sessionTracker != null) {
        	Log.i(TAG, "getExpirationDate: sessionTracker not null");
            Session currentSession = sessionTracker.getOpenSession();
            return (currentSession != null) ? currentSession.getExpirationDate() : null;
        }
        return null;
    }
    
    /**
     * Closes the current session.
     */
    protected final void closeSession() {
    	Log.i(TAG, "closeSession");
        if (sessionTracker != null) {
        	Log.i(TAG, "closeSession:sessionTracker not null");
            Session currentSession = sessionTracker.getOpenSession();
            if (currentSession != null) {
            	Log.i(TAG, "closeSession: currentSession not null");
                currentSession.close();
            }
        }
    }
    
    /**
     * Closes the current session as well as clearing the token cache.
     */
    protected final void closeSessionAndClearTokenInformation() {
    	Log.i(TAG, "closeSessionAndClearTokenInformation");
        if (sessionTracker != null) {
        	Log.i(TAG, "closeSessionAndClearTokenInformation: sessionTracker not null");
            Session currentSession = sessionTracker.getOpenSession();
            if (currentSession != null) {
            	Log.i(TAG, "closeSessionAndClearTokenInformation: currentSession not null");
                currentSession.closeAndClearTokenInformation();
            }
        }
    }
    
    /**
     * Gets the permissions associated with the current session or null if no session 
     * has been created.
     * 
     * @return the permissions associated with the current session
     */
    protected final List<String> getSessionPermissions() {
    	Log.i(TAG, "getSessionPermissions");
        if (sessionTracker != null) {
            Session currentSession = sessionTracker.getSession();
            Log.i(TAG, "openSessionForPublish:" + currentSession.getPermissions().toString());
            return (currentSession != null) ? currentSession.getPermissions() : null;
        }
        return null;
    }

    /**
     * Opens a new session. This method will use the application id from
     * the associated meta-data value and an empty list of permissions.
     */
    protected final void openSession() {
    	Log.i(TAG, "openSession");
        openSessionForRead(null, null);
    }

    /**
     * Opens a new session with read permissions. If either applicationID or permissions
     * is null, this method will default to using the values from the associated
     * meta-data value and an empty list respectively.
     *
     * @param applicationId the applicationID, can be null
     * @param permissions the permissions list, can be null
     */
    protected final void openSessionForRead(String applicationId, List<String> permissions) {
    	Log.i(TAG, "OpenSessionForRead (2 params)");
        openSessionForRead(applicationId, permissions, SessionLoginBehavior.SSO_WITH_FALLBACK,
                Session.DEFAULT_AUTHORIZE_ACTIVITY_CODE);
    }

    /**
     * Opens a new session with read permissions. If either applicationID or permissions
     * is null, this method will default to using the values from the associated
     * meta-data value and an empty list respectively.
     *
     * @param applicationId the applicationID, can be null
     * @param permissions the permissions list, can be null
     * @param behavior the login behavior to use with the session
     * @param activityCode the activity code to use for the SSO activity
     */
    protected final void openSessionForRead(String applicationId, List<String> permissions,
            SessionLoginBehavior behavior, int activityCode) {
    	Log.i(TAG, "openSessionForRead (4 params)");
        openSession(applicationId, permissions, behavior, activityCode, SessionAuthorizationType.READ);
    }

    /**
     * Opens a new session with publish permissions. If either applicationID is null,
     * this method will default to using the value from the associated
     * meta-data value. The permissions list cannot be null.
     *
     * @param applicationId the applicationID, can be null
     * @param permissions the permissions list, cannot be null
     */
    protected final void openSessionForPublish(String applicationId, List<String> permissions) {
    	Log.i(TAG, "openSessionForPublish (2 params)");
        openSessionForPublish(applicationId, permissions, SessionLoginBehavior.SSO_WITH_FALLBACK,
                Session.DEFAULT_AUTHORIZE_ACTIVITY_CODE);
    }

    /**
     * Opens a new session with publish permissions. If either applicationID is null,
     * this method will default to using the value from the associated
     * meta-data value. The permissions list cannot be null.
     *
     * @param applicationId the applicationID, can be null
     * @param permissions the permissions list, cannot be null
     * @param behavior the login behavior to use with the session
     * @param activityCode the activity code to use for the SSO activity
     */
    protected final void openSessionForPublish(String applicationId, List<String> permissions,
            SessionLoginBehavior behavior, int activityCode) {
    	Log.i(TAG, "openSessionForPublish (4 params)");
        openSession(applicationId, permissions, behavior, activityCode, SessionAuthorizationType.PUBLISH);
    }

    public void openSession(String applicationId, List<String> permissions,
            SessionLoginBehavior behavior, int activityCode, SessionAuthorizationType authType) {
    	Log.i(TAG, "openSession (param 5)");
        if (sessionTracker != null) {
        	Log.i(TAG, "openSession:sessionTracker is not null");
            Session currentSession = sessionTracker.getSession();
            if (currentSession == null || currentSession.getState().isClosed()) {
            	Log.i(TAG, "openSession:currentSession is null or closed");
                Session session = new Session.Builder(Login.this).setApplicationId(applicationId).build();
                Session.setActiveSession(session);
                currentSession = session;
            }
            if (!currentSession.isOpened()) {
            	Log.i(TAG, "openSession:currentSession is not Opened");
                Session.OpenRequest openRequest = new Session.OpenRequest(this).
                        setPermissions(permissions).
                        setLoginBehavior(behavior).
                        setRequestCode(activityCode);
                if (SessionAuthorizationType.PUBLISH.equals(authType)) {
                	Log.i(TAG, "openSession:publish settings");
                    currentSession.openForPublish(openRequest);
                } else {
                	Log.i(TAG, "openSession:read only settings");
                    currentSession.openForRead(openRequest);
                }
            }
        }
    }

    /**
     * The default callback implementation for the session.
     */
    public class DefaultSessionStatusCallback implements Session.StatusCallback {

        @Override
        public void call(Session session, 
                         SessionState state,
                         Exception exception) {
            Login.this.onSessionStateChange(state, exception);
        }
        
    }
    /**
     * request information
     */
    public void requestInfo(String graphPath, Bundle param){
    	Session currentSession = sessionTracker.getOpenSession();
    	Log.i(TAG, "requestInfo: " + currentSession.getState().toString());
    	if(currentSession.isOpened()){
    	/* make the API call */
    	new Request(
    		currentSession,
    		graphPath,
    		param,
    	    HttpMethod.GET,
    	    new Request.Callback() {
    	        public void onCompleted(Response response) {
    	            /* handle the result */
    	        	Log.i(TAG, "requestInfo: onComplete");
					// Process the returned response
                    GraphObject graphObject = response.getGraphObject();
                    FacebookRequestError error = response.getError();
                    String Data = graphObject.getProperty("data").toString();
                    dataDisplay.setText(Data);
    	        }
    	    }
    	).executeAsync();
    	}
    }
    
    public void pushInfo(String graphPath, Bundle param){
    	Session currentSession = sessionTracker.getOpenSession();
    	Log.i(TAG, "requestInfo: " + currentSession.getState().toString());
    	if(currentSession.isOpened()){
    	/* make the API call */
    		Request request;
    	new Request(
    			currentSession,
    			graphPath,
	    	    param,
	    	    HttpMethod.POST,
	    	    new Request.Callback() {
    	        public void onCompleted(Response response) {
    	            /* handle the result */
    	        	Log.i(TAG, "requestInfo: onComplete");
					// Process the returned response
                    //GraphObject graphObject = response.getGraphObject();
                    //FacebookRequestError error = response.getError();
                    dataDisplay.setText(response.toString());
    	        }
    	    }
    	).executeAsync();}
    }
    
    /**
     * Creates a facebook feed post
     *
     * @param name is the main title next to the picture
     * @param caption is the title caption
     * @param description is the description
     * @param link is the url link for the image
     * @param pictureURL is a http:// address to a picture
     */
    public void feedPost(String name, String caption, String description, String link, String pictureURL){
    	Bundle parameters = new Bundle();
        parameters.putString("name", name);
    	parameters.putString("caption", caption);
    	parameters.putString("description", description);
    	parameters.putString("link", link);
    	parameters.putString("picture", pictureURL);
    	pushInfo("/v1.0/me/feed",parameters);
    }
    
    /**
     * Creates Facebook event
     *
     * @param name
     * @param startDateAndTime //Example: '2012-07-04T19:00:00' to represent July 4th, 2012 at 7PM.
     * @param endDateAndTime   //Example: '2012-07-04T19:00:00' to represent July 4th, 2012 at 7PM.
     * @param eventDescription
     * @param eventLocation
     */
    public void createEvent(String eventName, String startDateAndTime, String endDateAndTime, String eventDescription, String eventLocation){
    	Bundle parameters = new Bundle();
    	parameters.putString("name", eventName);
    	parameters.putString("start_time", startDateAndTime);
    	parameters.putString("end_time", endDateAndTime);
    	parameters.putString("description", eventDescription);
    	parameters.putString("location", eventLocation);
    	parameters.putString("privacy_type", "OPEN");
    	pushInfo("/me/events",parameters);
    }
    
    public void testRequest(){
    	
    	Bundle params = new Bundle();
    	params.putString("event", "http://samples.ogp.me/839107789462360");

    	Request request = new Request(
    	    Session.getActiveSession(),
    	    "me/syncupgraphspace:create",
    	    params,
    	    HttpMethod.POST
    	);
    	Response response = request.executeAndWait();	
    	 dataDisplay.setText(response.toString());
    }
    
}
		
