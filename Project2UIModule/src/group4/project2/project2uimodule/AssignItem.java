package group4.project2.project2uimodule;

import group4.project2.project2uimodule.SessionData.EventItem;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Intent;
import android.content.ClipData.Item;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.TextView;
import android.widget.Toast;

public class AssignItem extends Activity {
	//UI Module Global Variables
	public Button backButton;
	public ListView assignItemListView;
	public SessionData sessionData;
	//End UI Global Variables

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_assign_item);
		
		//UI Module onCreate functionality
		final Intent backIntent = new Intent(this, EventItems.class);
		
		Bundle extras = getIntent().getExtras();
		if (extras != null) {
		   sessionData = (SessionData)extras.getSerializable("Session Data");
		}
		
		backButton = (Button)findViewById(R.id.assignItemBackButton);
		assignItemListView = (ListView)findViewById(R.id.assignItemListView);
		
		backButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				backIntent.putExtra("Session Data", sessionData);
				startActivity(backIntent);
				finish();
			}
		});
		
		assignItemListView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
					EventItem[] tempItemArray = 
	new EventItem[sessionData.syncedEvents[sessionData.selectedEvent].eventGuests[sessionData.selectedGuest].guestItems.length +1];
					
					for(int j = 0; j < sessionData.syncedEvents[sessionData.selectedEvent].eventGuests[sessionData.selectedGuest].guestItems.length; j++){
						if(sessionData.syncedEvents[sessionData.selectedEvent].eventGuests[sessionData.selectedGuest].guestItems[j].itemName.equals(
								sessionData.syncedEvents[sessionData.selectedEvent].eventItems[sessionData.selectedItem].itemName)){
							backIntent.putExtra("Session Data", sessionData);
							startActivity(backIntent);
							finish();
							return;
						}
					}
					for(int i = 0; i < tempItemArray.length; i++){
						if(i < sessionData.syncedEvents[sessionData.selectedEvent].eventGuests[sessionData.selectedGuest].guestItems.length)
							tempItemArray[i] = sessionData.syncedEvents[sessionData.selectedEvent].eventGuests[sessionData.selectedGuest].guestItems[i];
						else
							tempItemArray[i] = sessionData.syncedEvents[sessionData.selectedEvent].eventItems[sessionData.selectedItem];
					}
					sessionData.syncedEvents[sessionData.selectedEvent].eventGuests[sessionData.selectedGuest].guestItems = tempItemArray;
					backIntent.putExtra("Session Data", sessionData);
					startActivity(backIntent);
					finish();
			}
		});
		
		 	ArrayList<String> eventGuestsList = new ArrayList<String>();
	      
	      
		    ArrayAdapter<String> listAdapter = new ArrayAdapter<String>(this, R.layout.simple_row, eventGuestsList); 
		    for(int i=0; i<sessionData.syncedEvents[sessionData.selectedEvent].eventGuests.length; ++i){
		    	listAdapter.add( sessionData.syncedEvents[sessionData.selectedEvent].eventGuests[i].guestName);
		    }
		    assignItemListView.setAdapter(listAdapter);
		//End UI Module onCreate functionality
	}
}
