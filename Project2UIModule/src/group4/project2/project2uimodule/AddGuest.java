package group4.project2.project2uimodule;

import group4.project2.project2uimodule.SessionData.EventGuest;
import group4.project2.project2uimodule.SessionData.EventItem;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class AddGuest extends Activity {
	//UI Module Global Variables
		public EditText addGuestGuestNameEditText;
		public Button addGuestAddButton;
		public SessionData sessionData;
		//End UI Global Variables

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_add_guest);
		
		//UI Module onCreate functionality
		final Intent addGuestIntent = new Intent(this, CreateEvent.class);
		
		addGuestGuestNameEditText = (EditText)findViewById(R.id.addGuestGuestNameEditText);
		addGuestAddButton = (Button)findViewById(R.id.addGuestAddButton);
		
		Bundle extras = getIntent().getExtras();
		if (extras != null) {
		   sessionData = (SessionData)extras.getSerializable("Session Data");
		}
		
		addGuestAddButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				ValidateGuestName(addGuestIntent);
			}
		});
	}
	
	public void ValidateGuestName(Intent intent){
		if(addGuestGuestNameEditText.getText().toString().equals("")){
			Toast.makeText(this, "Invalid Guest Name", Toast.LENGTH_LONG).show();
		}
		else{
			EventGuest[] tempGuestArray = new EventGuest[sessionData.syncedEvents[sessionData.selectedEvent].eventGuests.length + 1];
			EventGuest tempGuest = new EventGuest();
			tempGuest.guestName = addGuestGuestNameEditText.getText().toString();
			for(int i = 0; i < tempGuestArray.length; i++){
				if(i<tempGuestArray.length-1){
					tempGuestArray[i] = sessionData.syncedEvents[sessionData.selectedEvent].eventGuests[i];
				}
				else{
					tempGuestArray[i] = tempGuest;
				}
			}
			sessionData.syncedEvents[sessionData.selectedEvent].eventGuests = tempGuestArray;
			intent.putExtra("Session Data", sessionData);
			startActivity(intent);
			finish();
		}
	}
}
