package group4.project2.project2uimodule;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;

public class EventGuests extends Activity {
	//UI Module Global Variables
	public Button backButton;
	public ListView eventGuestsListView;
	public SessionData sessionData;
	//End UI Global Variables

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_event_guests);
		
		//UI Module onCreate functionality
		final Intent backIntent = new Intent(this, EditEvent.class);
		final Intent isBringingIntent = new Intent(this, IsBringing.class);
		
		Bundle extras = getIntent().getExtras();
		if (extras != null) {
		   sessionData = (SessionData)extras.getSerializable("Session Data");
		}
		
		
		backButton = (Button)findViewById(R.id.eventGuestsBackButton);
		eventGuestsListView = (ListView)findViewById(R.id.eventGuestsListView);
		
		backButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				backIntent.putExtra("Session Data", sessionData);
				startActivity(backIntent);
				finish();
			}
		});
		
		eventGuestsListView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
					sessionData.selectedGuest = position;
					isBringingIntent.putExtra("Session Data", sessionData);
					startActivity(isBringingIntent);
					finish();
			}
		});
		
		ArrayList<String> eventGuestsList = new ArrayList<String>();    
	    ArrayAdapter<String> listAdapter = new ArrayAdapter<String>(this, R.layout.simple_row, eventGuestsList); 
	    for(int i=0; i<sessionData.syncedEvents[sessionData.selectedEvent].eventItems.length; ++i){
	    	listAdapter.add(sessionData.syncedEvents[sessionData.selectedEvent].eventGuests[i].guestName);
	    }
	    eventGuestsListView.setAdapter(listAdapter);
		//End UI Module onCreate functionality
	}
}
