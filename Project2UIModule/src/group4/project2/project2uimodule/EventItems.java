package group4.project2.project2uimodule;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;

public class EventItems extends Activity {
	//UI Module Global Variables
	public Button backButton;
	public ListView eventItemsListView;
	public SessionData sessionData;
	//End UI Global Variables

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_event_items);
		
		//UI Module onCreate functionality
		final Intent backIntent = new Intent(this, EditEvent.class);
		final Intent assignItemIntent = new Intent(this, AssignItem.class);
		
		Bundle extras = getIntent().getExtras();
		if (extras != null) {
		   sessionData = (SessionData)extras.getSerializable("Session Data");
		}
		
		backButton = (Button)findViewById(R.id.eventItemsBackButton);
		eventItemsListView = (ListView)findViewById(R.id.eventItemsListView);
		
		backButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				backIntent.putExtra("Session Data", sessionData);
				startActivity(backIntent);
				finish();
			}
		});
		
		eventItemsListView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
					sessionData.selectedItem = position;
					assignItemIntent.putExtra("Session Data", sessionData);
					startActivity(assignItemIntent);
					finish();
			}
		});
		
		ArrayList<String> eventItemsList = new ArrayList<String>();    
	    ArrayAdapter<String> listAdapter = new ArrayAdapter<String>(this, R.layout.simple_row, eventItemsList); 
	    for(int i=0; i<sessionData.syncedEvents[sessionData.selectedEvent].eventItems.length; ++i){
	    	listAdapter.add(sessionData.syncedEvents[sessionData.selectedEvent].eventItems[i].itemName);
	    }
	    eventItemsListView.setAdapter(listAdapter);
		//End UI Module onCreate functionality
}

}
