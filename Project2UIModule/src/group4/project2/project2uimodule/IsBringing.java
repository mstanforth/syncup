package group4.project2.project2uimodule;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;

public class IsBringing extends Activity {
	//UI Module Global Variables
	public Button backButton;
	public ListView isBringingListView;
	public SessionData sessionData;
	//End UI Global Variables

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_is_bringing);
		
		//UI Module onCreate functionality
		final Intent backIntent = new Intent(this, EventGuests.class);
		
		Bundle extras = getIntent().getExtras();
		if (extras != null) {
		   sessionData = (SessionData)extras.getSerializable("Session Data");
		}
		
		
		backButton = (Button)findViewById(R.id.isBringingBackButton);
		isBringingListView = (ListView)findViewById(R.id.isBringingListView);
		
		backButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				backIntent.putExtra("Session Data", sessionData);
				startActivity(backIntent);
				finish();
			}
		});
		
		isBringingListView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
			}
		});
		
		ArrayList<String> isBringingList = new ArrayList<String>();    
	    ArrayAdapter<String> listAdapter = new ArrayAdapter<String>(this, R.layout.simple_row, isBringingList); 
	    for(int i=0; i<sessionData.syncedEvents[sessionData.selectedEvent].eventGuests[sessionData.selectedGuest].guestItems.length; ++i){
	    	listAdapter.add(sessionData.syncedEvents[sessionData.selectedEvent].eventGuests[sessionData.selectedGuest].guestItems[i].itemName);
	    }
	    isBringingListView.setAdapter(listAdapter);
	}
}
