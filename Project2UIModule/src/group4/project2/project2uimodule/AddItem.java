package group4.project2.project2uimodule;

import group4.project2.project2uimodule.SessionData.EventItem;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class AddItem extends Activity {
	//UI Module Global Variables
	public EditText addItemItemNameEditText;
	public Button addItemAddButton;
	public SessionData sessionData;
	//End UI Global Variables

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_add_item);
		
		//UI Module onCreate functionality
		final Intent addItemIntent = new Intent(this, CreateEvent.class);
		
		addItemItemNameEditText = (EditText)findViewById(R.id.addItemItemNameEditText);
		addItemAddButton = (Button)findViewById(R.id.addItemAddButton);
		
		Bundle extras = getIntent().getExtras();
		if (extras != null) {
		   sessionData = (SessionData)extras.getSerializable("Session Data");
		}
		
		addItemAddButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				ValidateItemName(addItemIntent);
			}
		});
	}
	
	public void ValidateItemName(Intent intent){
		if(addItemItemNameEditText.getText().toString().equals("")){
			Toast.makeText(this, "Invalid Item Name", Toast.LENGTH_LONG).show();
		}
		else{
			EventItem[] tempItemArray = new EventItem[sessionData.syncedEvents[sessionData.selectedEvent].eventItems.length + 1];
			EventItem tempItem = new EventItem();
			tempItem.itemName = addItemItemNameEditText.getText().toString();
			for(int i = 0; i < tempItemArray.length; i++){
				if(i<tempItemArray.length-1){
					tempItemArray[i] = sessionData.syncedEvents[sessionData.selectedEvent].eventItems[i];
				}
				else{
					tempItemArray[i] = tempItem;
				}
			}
			sessionData.syncedEvents[sessionData.selectedEvent].eventItems = tempItemArray;
			intent.putExtra("Session Data", sessionData);
			startActivity(intent);
			finish();
		}
	}
}
