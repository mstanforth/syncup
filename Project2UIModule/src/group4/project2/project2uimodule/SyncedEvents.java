package group4.project2.project2uimodule;

import group4.project2.project2uimodule.SessionData.Event;
import group4.project2.project2uimodule.*;

import java.util.ArrayList;

import com.scringo.Scringo;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

public class SyncedEvents extends Activity {
	
	//UI Module Global Variables
	public Button logoutButton;
	public Button createEventButton;
	public ListView syncedEventsListView;
	public SessionData sessionData;
	//End UI Global Variables
	
	private Scringo scringo;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_synced_events);
		
		Scringo.setAppId("ZhbBOvc9ZxVrmGQjw4DZHNIvMsnS9N5R");
		scringo = new Scringo(this);
		scringo.init();
		scringo.addSidebar();
		
		//UI Module onCreate functionality
		final Intent logoutIntent = new Intent(this, Login.class);
		final Intent editEventIntent = new Intent(this, EditEvent.class);
		final Intent createEventIntent = new Intent(this, CreateEvent.class);
		
		Bundle extras = getIntent().getExtras();
		if (extras != null) {
		   sessionData = (SessionData)extras.getSerializable("Session Data");
		}
		
		logoutButton = (Button)findViewById(R.id.syncedEventsLogoutButton);
		createEventButton = (Button)findViewById(R.id.syncedEventsCreateEventButton);
		syncedEventsListView = (ListView)findViewById(R.id.syncedEventsListView);
		logoutButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				startActivity(logoutIntent);
				finish();
			}
		});
		
		createEventButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Event tempEvent = new Event();
				Event[] tempEventArray = new Event[sessionData.syncedEvents.length +1];
				for(int i = 0; i < tempEventArray.length; ++i){
					if(i<tempEventArray.length - 1){
						tempEventArray[i] = sessionData.syncedEvents[i];
					}
					else{
						tempEventArray[i] = tempEvent;
					}
				}
				sessionData.syncedEvents = tempEventArray;
				sessionData.selectedEvent = sessionData.syncedEvents.length-1;
				createEventIntent.putExtra("Session Data", sessionData);
				startActivity(createEventIntent);
				finish();
			}
		});
		
		syncedEventsListView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
					sessionData.selectedEvent = position;
					editEventIntent.putExtra("Session Data", sessionData);
					startActivity(editEventIntent);
					finish();
			}
		});  
	    ArrayList<String> syncedEventList = new ArrayList<String>();
	      
	    // Create ArrayAdapter using the planet list.  
	    ArrayAdapter<String> listAdapter = new ArrayAdapter<String>(this, R.layout.simple_row, syncedEventList); 
	    for(int i=0; i<sessionData.syncedEvents.length; ++i){
	    	listAdapter.add( sessionData.syncedEvents[i].eventName);
	    }
	    syncedEventsListView.setAdapter(listAdapter);
		
		//End UI Module onCreate functionality
	}
	
	@Override
	protected void onStart(){
		super.onStart();
		scringo.onStart();
	}
	
	@Override
	protected void onStop(){
		super.onStop();
		scringo.onStop();
	}
	
}
