package group4.project2.project2uimodule;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

public class EditEvent extends Activity {
	//UI Module Global Variables
	public EditText eventDesctiptionEditText;
	public Button viewEventItemsButton;
	public Button viewEventGuestsButton;
	public Button backButton;
	public SessionData sessionData;
	//End UI Global Variables

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_edit_event);
		
		//UI Module onCreate functionality
		final Intent backIntent = new Intent(this, SyncedEvents.class);
		final Intent eventItemsIntent = new Intent(this, EventItems.class);
		final Intent eventGuestsIntent = new Intent(this, EventGuests.class);
		
		Bundle extras = getIntent().getExtras();
		if (extras != null) {
		   sessionData = (SessionData)extras.getSerializable("Session Data");
		}
		
		backButton = (Button)findViewById(R.id.editEventBackButton);
		viewEventItemsButton = (Button)findViewById(R.id.viewItemListButton);
		viewEventGuestsButton = (Button)findViewById(R.id.viewGuestListButton);
		eventDesctiptionEditText = (EditText)findViewById(R.id.eventDescriptionEditText);
		
		if(sessionData.selectedEvent != -1){
			eventDesctiptionEditText.setText(sessionData.syncedEvents[sessionData.selectedEvent].eventDescription);
		}
		
		backButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				SubmitChanges();
				sessionData.selectedEvent = -1;
				backIntent.putExtra("Session Data", sessionData);
				startActivity(backIntent);
				finish();
			}
		});
		
		viewEventItemsButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				SubmitChanges();
				eventItemsIntent.putExtra("Session Data", sessionData);
				startActivity(eventItemsIntent);
				finish();
			}
		});
		viewEventGuestsButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				SubmitChanges();
				eventGuestsIntent.putExtra("Session Data", sessionData);
				startActivity(eventGuestsIntent);
				finish();
			}
		});
		
		//End UI Module onCreate functionality
	}
	
	public void SubmitChanges(){
		sessionData.syncedEvents[sessionData.selectedEvent].eventDescription = eventDesctiptionEditText.getText().toString();
	}
}
